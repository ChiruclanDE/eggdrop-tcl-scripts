bind raw - 354 qa_who
bind nick - * qa_nick
bind join - * qa_join
bind sign - * qa_sign
bind part - * qa_part
bind time - * qa_refresh

set GetAccount(version) 1.0
set GetAccount(author) "Chiruclan"

proc who {target} {
	putquick "WHO $target %cnat,321"
}

proc getauth {nick} {
	if {[info exists ::GetAccount(nick, $nick)]} {
		return $::GetAccount(nick, $nick)
	}
	return 0
}

proc getnick {auth} {
	if {[info exists ::GetAccount(auth, $auth)]} {
		return $::GetAccount(auth, $auth)
	}
	return 0
}

proc isauth {nick} {
	if {[info exists ::GetAccount(nick, $nick)]} {
		return 1
	}
	return 0
}

proc isonline {auth} {
	if {[info exists ::GetAccount(auth, $auth)]} {
		return 1
	}
	return 0
}

proc qa_who {from key text} {
	if {[lindex $text 1] == "321" && [llength $text] == "5"} {
		set chan [lindex $text 2]
		set nick [lindex $text 3]
		set auth [lindex $text 4]
		if {$auth != "0"} {
			if {[info exists ::GetAccount(protect, $nick)]} {
				unset ::GetAccount(protect, $nick)
			}
			set ::GetAccount(nick, $nick) $auth
			set ::GetAccount(auth, $auth) $nick
		} else {
			set ::GetAccount(protect, $nick) "1"
			utimer 600 [list unset ::GetAccount(protect, $nick)]
			if {[info exists ::GetAccount(nick, $nick)]} {
				unset ::GetAccount(nick, $nick)
			}
			if {[info exists ::GetAccount(auth, $auth)]} {
				unset ::GetAccount(auth, $auth)
			}
		}
	}
}

proc qa_nick {nick host handle chan newnick} {
	if {[isauth $nick]} {
		set ::GetAccount(nick, $newnick) [getauth $nick]
		set ::GetAccount(auth, [getauth $nick]) $newnick
		unset ::GetAccount(auth, [getauth $nick])
		unset ::GetAccount(nick, $nick)
	}
}

proc qa_join {nick uhost handle chan} {
	global botnick
	if {$nick == $botnick} {
		who $chan
	} else {
		who $nick
	}
}

proc qa_sign {nick uhost handle chan reason} {
	if {[isauth $nick]} {
		unset ::GetAccount(auth, [getauth $nick])
		unset ::GetAccount(nick, $nick)
	}
}

proc qa_part {nick uhost handle chan msg} {
	if {![onchan $nick]} {
		if {[isauth $nick]} {
			unset ::GetAccount(auth, [getauth $nick])
			unset ::GetAccount(nick, $nick)
		}
	}
}

proc qa_refresh {{args ""}} {
	foreach chan [channels] {
		foreach user [chanlist $chan] {
			if {![info exists ::GetAccount(protect, $user)]} {
				if {![isauth $user]} {
					who $user
				}
			}
		}
	}
}

putlog "% WHOX GetAccount script v$::GetAccount(version) by $::GetAccount(author) loaded!"