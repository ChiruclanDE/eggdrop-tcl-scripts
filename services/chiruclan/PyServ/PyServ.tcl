namespace eval PyServ {
	variable version 1.0
	variable author "Chiruclan"
	
	# Fill in your the Account data
	variable Account ""
	variable Password ""
	
	# Be sure that you'll your rights after you logged in.
	variable Sync 1
	
	# Please note that you should give the full Service like Nick@Hostname (or Nick@Servername to be exact)!
	# If you don't know what I mean, Nick@Servername is what is posted at HELP for AUTH command.
	variable Service "nick@hostname"
	
	bind EVNT  -|-   init-server   [namespace current]::Connect
}

proc PyServ::Connect {event} {
	putquick "PRIVMSG $PyServ::Service :AUTH $PyServ::Account $PyServ::Password"
	if {$PyServ::Sync} {
		putquick "PRIVMSG $PyServ::Service :SYNC"
	}
}

putlog "% PyServ authentication script v$PyServ::Version by $PyServ::Author loaded!"